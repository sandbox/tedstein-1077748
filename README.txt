This module is for calling a PHP script (for you to define) to migrate data.

Custom Migration just creates a page with a button to call the script.

_You write the script._

Great starting point, avoids need to write any code other than the migration script.

Credit to Darrell Duane for creating the initial module.

***********************************************************
Features
***********************************************************

- Creates a single button, which calls a function you define.

***********************************************************
Requirements and dependencies
***********************************************************

- None

***********************************************************
Permissions
***********************************************************

Unless you are user #1, you will not be able to access the page.

Permissions:

- administer migration -- Needed for access to page with button.

***********************************************************
Instructions
***********************************************************

- Modify the last function in custom_migration.module (custom_migration_action()).

- Install and enable the module.

- Clear the cache (a menu item is created).

- Run your custom function by navigating to:

  /admin/migration

  and clicking the button labeled 'Migration Action.'

- Delete the module from your site once your migration is complete

***********************************************************
Tips
***********************************************************

- Take your site offline before running your script.

- Back up your database before running your script.
